package ru.t1.chernysheva.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.chernysheva.tm.dto.model.AbstractModelDTO;
import ru.t1.chernysheva.tm.model.AbstractModel;

@NoRepositoryBean
public interface IModelRepository<M extends AbstractModel> extends JpaRepository<M, String> {
}
