package ru.t1.chernysheva.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.chernysheva.tm.api.service.dto.ITaskDTOService;
import ru.t1.chernysheva.tm.dto.soap.*;
import ru.t1.chernysheva.tm.model.CustomUser;
import ru.t1.chernysheva.tm.util.UserUtil;

@Endpoint
public class TaskSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.chernysheva.t1.ru/dto/soap";


    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findOne(
            @RequestPayload final TaskFindByIdRequest request) {
        return new TaskFindByIdResponse(taskService.findOneById(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse saveOne(
            @RequestPayload final TaskSaveRequest request) {
        taskService.save(UserUtil.getUserId(), request.getTask());
        return new TaskSaveResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskUpdateRequest", namespace = NAMESPACE)
    public TaskUpdateResponse updateOne(
            @RequestPayload final TaskUpdateRequest request) {
        taskService.save(UserUtil.getUserId(), request.getTask());
        return new TaskUpdateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse deleteOne(
            @RequestPayload final TaskDeleteRequest request) {
        taskService.removeOneById(UserUtil.getUserId(), request.getId());
        return new TaskDeleteResponse();
    }

}

