package ru.t1.chernysheva.tm.exception;

public final class AuthException extends AbstractException {

    public AuthException() {
        super("Error! Credentials are wrong...");
    }

}

