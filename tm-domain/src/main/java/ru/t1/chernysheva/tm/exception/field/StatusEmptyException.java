package ru.t1.chernysheva.tm.exception.field;

public final class StatusEmptyException extends AbstractFieldException {

    public StatusEmptyException() {
        super("Error! This status does not exist in the system...");
    }

}
