package ru.t1.chernysheva.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.chernysheva.tm.api.repository.ICommandRepository;
import ru.t1.chernysheva.tm.api.service.ICommandService;
import ru.t1.chernysheva.tm.listener.AbstractListener;

import java.util.Collection;

@Service
@NoArgsConstructor(force = true)
@AllArgsConstructor
public final class CommandService implements ICommandService {

    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @Override
    public void add(@Nullable final AbstractListener command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Nullable
    @Override
    public AbstractListener getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    @Nullable
    @Override
    public AbstractListener getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    @NotNull
    public Iterable<AbstractListener> getCommandsWithArgument() {
        return commandRepository.getCommandsWithArgument();
    }

}
