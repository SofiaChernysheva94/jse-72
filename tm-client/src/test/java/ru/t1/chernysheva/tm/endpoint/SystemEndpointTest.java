package ru.t1.chernysheva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chernysheva.tm.api.endpoint.ISystemEndpoint;
import ru.t1.chernysheva.tm.dto.request.ApplicationVersionRequest;
import ru.t1.chernysheva.tm.marker.SoapCategory;

@Category(SoapCategory.class)
public class SystemEndpointTest {

    @NotNull
    private final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance();

    @Test
    public void testGetApplicationVersion() {
        Assert.assertNotNull(systemEndpoint.getVersion(new ApplicationVersionRequest()));
    }

}
